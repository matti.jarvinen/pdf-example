import inlineCss from "inline-css";
import { inlineSource } from "inline-source";
import puppeteer from "puppeteer";

const generatePdf = async (html, headerFn, footerFn, pdfName) => {
  const browser = await puppeteer.launch({ headless: true });
  const page = await browser.newPage();

  await page.setContent(html, {
    waitUntil: "networkidle2",
  });
  //await page.emulateMedia('screen');

  const header = await headerFn(page);
  const footer = await footerFn(page);

  await page.pdf({
    path: pdfName,
    format: "A4",
    printBackground: true,
    displayHeaderFooter: true,
    // must fit header and footer
    margin: {
      top: 120,
      bottom: 80,
    },
    headerTemplate: header,
    footerTemplate: footer,
  });

  await browser.close();
};

(async () => {
  const headerFn = async (page) => {
    const title = await page.title();

    const x = await inlineSource(
      `<div style="width: 100%; display: flex; flex-direction: row; align-items: center;font-size: 10px; color: #666; margin: 20px;">
        <img style="flex-grow: 0;max-width: 120px; height: auto;"
        src="data:image/svg+xml;UTF8,%0A%3Csvg id='Layer_1' data-name='Layer 1' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 1716.64 391.93'%3E%3Cdefs%3E%3Cstyle%3E.cls-1%7Bfill:%23346094;%7D%3C/style%3E%3C/defs%3E%3Cpath class='cls-1' d='M449.41,100c0,13.59-1.72,22.65-6.09,28.42-5.31,7.34-14.53,10.78-27.18,11.09-3.12.16-31.54,0-31.54,0v44.82H363.83V60.67H416.3c13.59,0,21.71,3.9,26.55,10.46C447.06,77.06,449.41,86.12,449.41,100Zm-20.93.31c0-7.18-.62-12.33-1.87-14.68-2.19-4.21-7-6.4-14.53-6.4H384.44V121h27.17c8.75,0,13-2.5,15.15-6.72C428.17,111.42,428.48,107.52,428.48,100.33Z'/%3E%3Cpath class='cls-1' d='M531.61,141.09c0,12.65-2.66,22.65-8.28,30.45-6.25,8.91-17.18,14.06-32,14.06-15,0-25.61-5-32.17-13.74-5.15-6.87-8.27-18.12-8.27-30.77,0-12,2.81-23,8.43-30.45,6.25-8.28,16.4-14,32.17-14,15.62,0,25.92,5.46,31.86,13.74C528.79,117.67,531.61,128.44,531.61,141.09Zm-18.59.16c0-7.65-1.09-15.93-4.21-20.3-3.28-4.69-8.75-7-17.34-7-8.43,0-14.05,2.81-17.33,8-3,4.53-4.22,12.5-4.22,19.37,0,8.12,1.41,15.46,4.06,19.68,3.44,5.46,9.37,7.65,17.49,7.65,7.81,0,13.74-2.34,17.18-7.65C511.3,156.55,513,149.06,513,141.25Z'/%3E%3Cpath class='cls-1' d='M613.12,160.14c0,16.56-11.08,25.62-35.29,25.62-23.27,0-34.82-5.31-37.32-18.12a54.34,54.34,0,0,1-.78-9.68h18.74a32.64,32.64,0,0,0,.47,6.09c1.09,4.68,7.49,6.25,18.89,6.25,12.34,0,16.71-3.28,16.71-9.53,0-6.09-4.84-7.81-15.93-10.78-3.75-1.09-11.87-3.28-15.15-4.06-14.83-3.9-22.64-10.77-22.64-25.76,0-13.75,11.24-23.58,33.89-23.58,20.46,0,31.39,5.93,34.51,16.86.94,3,1.09,9.22,1.09,11.25H592c0-1.1,0-4.69-.78-6.41-1.56-3.28-5.31-5.46-16.08-5.46-11.4,0-15.62,2.65-15.62,8.12,0,5,3,7.18,13.43,9.84,4.22,1.09,11.56,3,15.77,4.06C605.16,139.06,613.12,145.62,613.12,160.14Z'/%3E%3Cpath class='cls-1' d='M645.45,89.87h-20.3V70.19h20.3Zm.31,94.48h-19.2c-.78-.31-.94-2.34-.94-7.81V98.46H645v77.61C645,181.54,645.3,183.88,645.76,184.35Z'/%3E%3Cpath class='cls-1' d='M707.39,115.33H686.62v43.25c0,8,3.28,9.84,10.46,9.84a57.58,57.58,0,0,0,9.52-.94v16.25c-3.43.93-10,1.56-16.86,1.56-11.87,0-22.49-5.16-22.49-21.08V115.33H655.54V98.46h11.4c.47-1.41.78-17.18.78-17.18L686.3,81V98.46h21.09Z'/%3E%3Cpath class='cls-1' d='M738.31,89.87H718V70.19h20.31Zm.31,94.48H719.41c-.78-.31-.94-2.34-.94-7.81V98.46h19.37v77.61C737.84,181.54,738.15,183.88,738.62,184.35Z'/%3E%3Cpath class='cls-1' d='M773.91,89.87h-20.3V70.19h20.3Zm.31,94.48H755c-.78-.31-.93-2.34-.93-7.81V98.46h19.36v77.61C773.44,181.54,773.75,183.88,774.22,184.35Z'/%3E%3Cpath class='cls-1' d='M866.83,98.46l-30.14,85.89H813.26L783,98.46h20.3l21.86,67.15,22.33-67.15Z'/%3E%3Cpath class='cls-1' d='M896.19,89.87h-20.3V70.19h20.3Zm.31,94.48H877.29c-.78-.31-.93-2.34-.93-7.81V98.46h19.36v77.61C895.72,181.54,896,183.88,896.5,184.35Z'/%3E%3Cpath class='cls-1' d='M984,184.35H965.21c-.93,0-.93-1.56-.93-55,0-9.37-4.85-14.52-15-14.52-9.52,0-18.11,5.15-18.11,16.24v53.25H912V98.46h18.58V110c3.91-9.22,14.84-13.28,26.08-13.28,18.9,0,26.86,9.68,26.86,27.17C983.48,183.41,983.64,184.35,984,184.35Z'/%3E%3Cpath class='cls-1' d='M1072,156.87a35,35,0,0,1-10.62,20c-6.56,5.94-15.77,8.91-27.49,8.91-14.83,0-26.7-4.69-33.57-15.93-4.06-6.41-6.4-15.78-6.4-28.58,0-12,2.34-21.4,6.4-27.95,6.4-10.16,17.33-16.71,33.42-16.71,16.55,0,27.17,6.4,32.79,15.77,5.16,8.43,6.87,20.61,5.94,36.23h-59.81a25.35,25.35,0,0,0,4.52,14.21c3,4.22,9.22,6.72,16.71,6.72,6.41,0,11.56-1.41,14.68-4.07a13.2,13.2,0,0,0,5-8.58Zm-18.27-23.12c0-3.74-1.1-10.62-3.75-13.9-3.12-3.9-8.28-6.71-16.4-6.71-7.49,0-12.8,2-16.55,6.25-3,3.28-4.53,8.58-4.69,14.36Z'/%3E%3Cpath class='cls-1' d='M1155.57,184.35h-18.74c-.94,0-.94-1.56-.94-55,0-9.37-4.84-14.52-15-14.52-9.53,0-18.12,5.15-18.12,16.24v53.25h-19.21V98.46h18.59V110c3.9-9.22,14.83-13.28,26.08-13.28,18.89,0,26.86,9.68,26.86,27.17C1155.1,183.41,1155.25,184.35,1155.57,184.35Z'/%3E%3Cpath class='cls-1' d='M383.76,336.56H364.55c-.47-.31-.63-1.72-.63-6.09V220.84c0-3.59-.15-7.34-.31-8.75h19.05a30.33,30.33,0,0,1,.63,7.19V328.91C383.29,333.28,383.29,335.78,383.76,336.56Z'/%3E%3Cpath class='cls-1' d='M451.38,336.56l-.47-10.31c-5,8.43-15.62,11.56-26.08,11.56-15.77,0-26.86-7.34-26.86-23.9,0-45.75-.16-62.77-.63-63.24h19.37c.31,0,.47,25.92.47,57.78,0,9.84,6.24,13.58,15.14,13.58,11.25,0,18.12-5.77,18.12-15.14V250.67h18.9v73.08a124.21,124.21,0,0,0,.46,12.81Z'/%3E%3Cpath class='cls-1' d='M561.47,293.3c0,12.65-2.65,22.64-8.27,30.45-6.25,8.9-17.18,14.06-32,14.06s-25.61-5-32.17-13.74c-5.15-6.88-8.27-18.12-8.27-30.77,0-12,2.81-23,8.43-30.45,6.25-8.28,16.4-14.06,32.17-14.06,15.62,0,25.92,5.47,31.86,13.75C558.66,269.88,561.47,280.65,561.47,293.3Zm-18.58.16c0-7.66-1.09-15.93-4.22-20.3-3.28-4.69-8.74-7-17.33-7-8.43,0-14.06,2.81-17.34,8-3,4.53-4.21,12.5-4.21,19.37,0,8.12,1.4,15.46,4.06,19.67,3.43,5.47,9.37,7.66,17.49,7.66,7.81,0,13.74-2.35,17.18-7.66C541.17,308.76,542.89,301.26,542.89,293.46Z'/%3E%3Cpath class='cls-1' d='M665,267.53V250.67H643.87V233.18l-18.58.31s-.31,15.77-.78,17.18H596V233.18l-18.59.31s-.31,15.77-.78,17.18h-11.4v16.86h11.72v48.88c0,15.93,10.61,21.09,22.48,21.09,6.87,0,13.43-.63,16.87-1.57V319.69a57.7,57.7,0,0,1-9.53.94c-7.18,0-10.46-1.87-10.46-9.84V267.53h28.52v48.88c0,15.93,10.62,21.09,22.49,21.09,6.87,0,13.43-.63,16.86-1.57V319.69a57.69,57.69,0,0,1-9.52.94c-7.19,0-10.47-1.87-10.47-9.84V267.53Z'/%3E%3Cpath class='cls-1' d='M751.47,293.3c0,12.65-2.65,22.64-8.28,30.45-6.24,8.9-17.18,14.06-32,14.06-15,0-25.61-5-32.17-13.74-5.15-6.88-8.28-18.12-8.28-30.77,0-12,2.81-23,8.44-30.45,6.24-8.28,16.39-14.06,32.17-14.06,15.61,0,25.92,5.47,31.85,13.75C748.66,269.88,751.47,280.65,751.47,293.3Zm-18.58.16c0-7.66-1.1-15.93-4.22-20.3-3.28-4.69-8.75-7-17.33-7s-14.06,2.81-17.34,8c-3,4.53-4.22,12.5-4.22,19.37,0,8.12,1.41,15.46,4.06,19.67,3.44,5.47,9.37,7.66,17.5,7.66,7.8,0,13.74-2.35,17.17-7.66C731.17,308.76,732.89,301.26,732.89,293.46Z'/%3E%3Cpath class='cls-1' d='M807.07,267.53H786.3v43.26c0,8,3.28,9.84,10.46,9.84a57.58,57.58,0,0,0,9.52-.94v16.24c-3.43.94-10,1.57-16.86,1.57-11.87,0-22.49-5.16-22.49-21.09V267.53H755.22V250.67h11.4c.47-1.41.78-17.18.78-17.18l18.58-.31v17.49h21.09Z'/%3E%3Cpath class='cls-1' d='M837.05,242.08h-20.3V222.4h20.3Zm.31,94.48H818.15c-.78-.31-.93-2.34-.93-7.81V250.67h19.36v77.61C836.58,333.75,836.89,336.09,837.36,336.56Z'/%3E%3Cpath class='cls-1' d='M926.69,309.07a35.12,35.12,0,0,1-10.62,20c-6.56,5.94-15.78,8.9-27.49,8.9-14.83,0-26.7-4.68-33.57-15.93-4.06-6.4-6.41-15.77-6.41-28.57,0-12,2.35-21.4,6.41-28,6.4-10.15,17.33-16.71,33.42-16.71,16.55,0,27.17,6.41,32.79,15.78,5.15,8.43,6.87,20.61,5.94,36.23H867.34A25.47,25.47,0,0,0,871.87,315c3,4.21,9.22,6.71,16.71,6.71,6.4,0,11.56-1.4,14.68-4.06a13.2,13.2,0,0,0,5-8.59ZM908.42,286c0-3.75-1.1-10.62-3.75-13.9-3.13-3.9-8.28-6.71-16.4-6.71-7.5,0-12.81,2-16.55,6.24-3,3.28-4.53,8.59-4.69,14.37Z'/%3E%3Cpath class='cls-1' d='M981.34,267.53H960.57v43.26c0,8,3.28,9.84,10.47,9.84a57.69,57.69,0,0,0,9.52-.94v16.24c-3.43.94-10,1.57-16.86,1.57-11.87,0-22.49-5.16-22.49-21.09V267.53H929.5V250.67h11.4c.46-1.41.78-17.18.78-17.18l18.58-.31v17.49h21.08Z'/%3E%3Cpath class='cls-1' d='M1067.86,293.3c0,12.65-2.65,22.64-8.28,30.45-6.24,8.9-17.18,14.06-32,14.06-15,0-25.61-5-32.17-13.74-5.15-6.88-8.28-18.12-8.28-30.77,0-12,2.81-23,8.44-30.45,6.24-8.28,16.39-14.06,32.17-14.06,15.61,0,25.92,5.47,31.85,13.75C1065.05,269.88,1067.86,280.65,1067.86,293.3Zm-18.58.16c0-7.66-1.1-15.93-4.22-20.3-3.28-4.69-8.75-7-17.33-7s-14.06,2.81-17.34,8c-3,4.53-4.22,12.5-4.22,19.37,0,8.12,1.41,15.46,4.06,19.67,3.44,5.47,9.37,7.66,17.5,7.66,7.8,0,13.74-2.35,17.17-7.66C1047.56,308.76,1049.28,301.26,1049.28,293.46Z'/%3E%3Cpath class='cls-1' d='M1134.39,250.2v19.52a56.14,56.14,0,0,0-16.09-2c-11.24,0-19.52,7.65-19.52,22.17v46.7h-19.36V250.67h18.89v13.12c2-8.44,12.65-15,23.58-15A46.44,46.44,0,0,1,1134.39,250.2Z'/%3E%3Cpath class='cls-1' d='M1215.28,309.07a35,35,0,0,1-10.62,20c-6.56,5.94-15.77,8.9-27.48,8.9-14.84,0-26.71-4.68-33.58-15.93-4.06-6.4-6.4-15.77-6.4-28.57,0-12,2.34-21.4,6.4-28,6.41-10.15,17.34-16.71,33.42-16.71,16.56,0,27.18,6.41,32.8,15.78,5.15,8.43,6.87,20.61,5.93,36.23h-59.81a25.47,25.47,0,0,0,4.53,14.21c3,4.21,9.21,6.71,16.71,6.71,6.4,0,11.56-1.4,14.68-4.06a13.24,13.24,0,0,0,5-8.59ZM1197,286c0-3.75-1.09-10.62-3.75-13.9-3.12-3.9-8.27-6.71-16.39-6.71-7.5,0-12.81,2-16.56,6.24-3,3.28-4.53,8.59-4.68,14.37Z'/%3E%3Cpath class='cls-1' d='M1300.7,336.56h-21.23l-21.24-41.23-13.12,13.9v27.33H1225.9V212.09h19.21V285.8l31.39-35.13h21.86l-26.7,30.14Z'/%3E%3Cpath class='cls-1' d='M1327.56,242.08h-20.3V222.4h20.3Zm.32,94.48h-19.21c-.78-.31-.94-2.34-.94-7.81V250.67h19.37v77.61C1327.1,333.75,1327.41,336.09,1327.88,336.56Z'/%3E%3Cpath class='cls-1' d='M1412.83,312.35c0,16.56-11.09,25.61-35.29,25.61-23.27,0-34.83-5.31-37.33-18.11a55.36,55.36,0,0,1-.78-9.68h18.74a32.64,32.64,0,0,0,.47,6.09c1.09,4.68,7.5,6.24,18.9,6.24,12.33,0,16.71-3.28,16.71-9.52s-4.84-7.81-15.93-10.78c-3.75-1.09-11.87-3.28-15.15-4.06-14.84-3.9-22.64-10.77-22.64-25.77,0-13.74,11.24-23.58,33.88-23.58,20.46,0,31.39,5.94,34.52,16.87.93,3,1.09,9.21,1.09,11.24h-18.27c0-1.09,0-4.68-.78-6.4-1.56-3.28-5.31-5.47-16.09-5.47-11.4,0-15.61,2.66-15.61,8.13,0,5,3,7.18,13.43,9.83,4.21,1.1,11.55,3,15.77,4.06C1404.87,291.27,1412.83,297.83,1412.83,312.35Z'/%3E%3Cpath class='cls-1' d='M1468.11,267.53h-20.77v43.26c0,8,3.28,9.84,10.47,9.84a57.69,57.69,0,0,0,9.52-.94v16.24c-3.43.94-10,1.57-16.86,1.57-11.87,0-22.49-5.16-22.49-21.09V267.53h-11.71V250.67h11.4c.47-1.41.78-17.18.78-17.18l18.58-.31v17.49h21.08Z'/%3E%3Cpath class='cls-1' d='M1552,309.07a35.12,35.12,0,0,1-10.62,20c-6.56,5.94-15.78,8.9-27.49,8.9-14.83,0-26.7-4.68-33.57-15.93-4.06-6.4-6.41-15.77-6.41-28.57,0-12,2.35-21.4,6.41-28,6.4-10.15,17.33-16.71,33.42-16.71,16.55,0,27.17,6.41,32.79,15.78,5.15,8.43,6.87,20.61,5.93,36.23h-59.81a25.47,25.47,0,0,0,4.53,14.21c3,4.21,9.22,6.71,16.71,6.71,6.4,0,11.56-1.4,14.68-4.06a13.2,13.2,0,0,0,5-8.59ZM1533.7,286c0-3.75-1.09-10.62-3.74-13.9-3.13-3.9-8.28-6.71-16.4-6.71-7.5,0-12.81,2-16.55,6.24-3,3.28-4.53,8.59-4.69,14.37Z'/%3E%3Cpath class='cls-1' d='M1617.56,250.2v19.52a56,56,0,0,0-16.08-2c-11.24,0-19.52,7.65-19.52,22.17v46.7h-19.37V250.67h18.9v13.12c2-8.44,12.65-15,23.58-15A46.3,46.3,0,0,1,1617.56,250.2Z'/%3E%3Cpath class='cls-1' d='M1646.14,242.08h-20.3V222.4h20.3Zm.31,94.48h-19.2c-.78-.31-.94-2.34-.94-7.81V250.67h19.36v77.61C1645.67,333.75,1646,336.09,1646.45,336.56Z'/%3E%3Cpath class='cls-1' d='M94.43,305.48l64.17-64.17a2.05,2.05,0,0,1,3.5,1.45v90.87a3.78,3.78,0,0,0,3.42,3.76q6.3.57,12.74.57t12.73-.57a3.77,3.77,0,0,0,3.42-3.76V186.43a3.77,3.77,0,0,0-3.77-3.77H44a3.76,3.76,0,0,0-3.75,3.41q-.63,6.54-.63,13.25,0,6.17.53,12.2A3.77,3.77,0,0,0,43.89,215h90.42a2.05,2.05,0,0,1,1.45,3.5L71.63,282.59a3.78,3.78,0,0,0-.24,5.07,140.49,140.49,0,0,0,18,18A3.77,3.77,0,0,0,94.43,305.48Z'/%3E%3Cpath class='cls-1' d='M306.47,146.45A139,139,0,0,0,292.67,121a3.79,3.79,0,0,0-3.11-1.64H261.49a3.76,3.76,0,0,1-3.76-3.76V87.67a3.76,3.76,0,0,0-1.66-3.12,138.63,138.63,0,0,0-25.46-13.66,3.77,3.77,0,0,0-5.19,3.49v73.5a3.77,3.77,0,0,0,3.77,3.77H303A3.76,3.76,0,0,0,306.47,146.45Z'/%3E%3Cpath class='cls-1' d='M132.4,147.88v-74a3.77,3.77,0,0,0-5.15-3.51,138.4,138.4,0,0,0-25.46,13.3,3.76,3.76,0,0,0-1.69,3.14v28.79a3.76,3.76,0,0,1-3.77,3.76H67A3.8,3.8,0,0,0,63.84,121,139,139,0,0,0,50,146.45a3.77,3.77,0,0,0,3.49,5.2h75.11A3.76,3.76,0,0,0,132.4,147.88Z'/%3E%3Cpath class='cls-1' d='M225.42,249.74v74.51a3.77,3.77,0,0,0,5.19,3.49,138,138,0,0,0,25.46-13.66,3.76,3.76,0,0,0,1.66-3.12V282.05a3.76,3.76,0,0,1,3.76-3.77h28.75a3.76,3.76,0,0,0,3.13-1.66,138.49,138.49,0,0,0,13.52-25.47,3.77,3.77,0,0,0-3.5-5.17h-74.2A3.76,3.76,0,0,0,225.42,249.74Z'/%3E%3Cpath class='cls-1' d='M194.41,147.88V65A3.75,3.75,0,0,0,191,61.25q-6.28-.59-12.73-.58t-12.74.58A3.76,3.76,0,0,0,162.1,65v82.88a3.77,3.77,0,0,0,3.77,3.77h24.77A3.77,3.77,0,0,0,194.41,147.88Z'/%3E%3Cpath class='cls-1' d='M312.53,182.66H229.19a3.77,3.77,0,0,0-3.77,3.77V211.2a3.77,3.77,0,0,0,3.77,3.77h83.43a3.77,3.77,0,0,0,3.75-3.45q.54-6,.54-12.2c0-4.47-.22-8.89-.63-13.25A3.77,3.77,0,0,0,312.53,182.66Z'/%3E%3C/svg%3E" />
        <div style="flex-grow: 1; text-align: right;">${title}</div>
      </div>`,
      {
        svgAsImage: true,
      }
    );

    return x;
  };
  const footerFn = async (page) =>
    `<div style="width: 100%; display: flex; flex-direction: row; align-items: center;font-size: 10px; color: #666; margin: 20px;">
      
      <div style="flex-grow: 1; text-align: center">Footer Template</div>
      <div style="flex-grow: 0"><span class="pageNumber"></span>/<span class="totalPages"></span></div>
    </div>`;

  const html = `
<!DOCTYPE html>
<html lang="fi">
  <head>
    <title>PDF from HTML</title>
    <meta charset="utf-8" />
	<style>
        h1 {
            color: #fff;
            padding: 10pt;
            background: red;
        }

        :root {
            --bs-blue: #0d6efd;
            --bs-indigo: #6610f2;
            --bs-purple: #6f42c1;
            --bs-pink: #d63384;
            --bs-red: #dc3545;
            --bs-orange: #fd7e14;
            --bs-yellow: #ffc107;
            --bs-green: #198754;
            --bs-teal: #20c997;
            --bs-cyan: #0dcaf0;
            --bs-black: #000;
            --bs-white: #fff;
            --bs-gray: #6c757d;
            --bs-gray-dark: #343a40;
            --bs-gray-100: #f8f9fa;
            --bs-gray-200: #e9ecef;
            --bs-gray-300: #dee2e6;
            --bs-gray-400: #ced4da;
            --bs-gray-500: #adb5bd;
            --bs-gray-600: #6c757d;
            --bs-gray-700: #495057;
            --bs-gray-800: #343a40;
            --bs-gray-900: #212529;
            --bs-primary: #0d6efd;
            --bs-secondary: #6c757d;
            --bs-success: #198754;
            --bs-info: #0dcaf0;
            --bs-warning: #ffc107;
            --bs-danger: #dc3545;
            --bs-light: #f8f9fa;
            --bs-dark: #212529;
            --bs-primary-rgb: 13, 110, 253;
            --bs-secondary-rgb: 108, 117, 125;
            --bs-success-rgb: 25, 135, 84;
            --bs-info-rgb: 13, 202, 240;
            --bs-warning-rgb: 255, 193, 7;
            --bs-danger-rgb: 220, 53, 69;
            --bs-light-rgb: 248, 249, 250;
            --bs-dark-rgb: 33, 37, 41;
            --bs-white-rgb: 255, 255, 255;
            --bs-black-rgb: 0, 0, 0;
            --bs-body-color-rgb: 33, 37, 41;
            --bs-body-bg-rgb: 255, 255, 255;
            --bs-font-sans-serif: system-ui, -apple-system, "Segoe UI", Roboto, "Helvetica Neue", "Noto Sans", "Liberation Sans", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
            --bs-font-monospace: SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace;
            --bs-gradient: linear-gradient(180deg, rgba(255, 255, 255, 0.15), rgba(255, 255, 255, 0));
            --bs-body-font-family: var(--bs-font-sans-serif);
            --bs-body-font-size: 1rem;
            --bs-body-font-weight: 400;
            --bs-body-line-height: 1.5;
            --bs-body-color: #212529;
            --bs-body-bg: #fff;
            --bs-border-width: 1px;
            --bs-border-style: solid;
            --bs-border-color: #dee2e6;
            --bs-border-color-translucent: rgba(0, 0, 0, 0.175);
            --bs-border-radius: 0.375rem;
            --bs-border-radius-sm: 0.25rem;
            --bs-border-radius-lg: 0.5rem;
            --bs-border-radius-xl: 1rem;
            --bs-border-radius-2xl: 2rem;
            --bs-border-radius-pill: 50rem;
            --bs-link-color: #0d6efd;
            --bs-link-hover-color: #0a58ca;
            --bs-code-color: #d63384;
            --bs-highlight-bg: #fff3cd;
          }

        table {
            caption-side: bottom;
            border-collapse: collapse;
        }
        
        caption {
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
            color: #6c757d;
            text-align: left;
        }
        
        th {
            text-align: inherit;
            text-align: -webkit-match-parent;
        }
        
        thead,
        tbody,
        tfoot,
        tr,
        td,
        th {
            border-color: inherit;
            border-style: solid;
            border-width: 0;
        }

        .table {
            --bs-table-color: var(--bs-body-color);
            --bs-table-bg: transparent;
            --bs-table-border-color: var(--bs-border-color);
            --bs-table-accent-bg: transparent;
            --bs-table-striped-color: var(--bs-body-color);
            --bs-table-striped-bg: rgba(0, 0, 0, 0.05);
            --bs-table-active-color: var(--bs-body-color);
            --bs-table-active-bg: rgba(0, 0, 0, 0.1);
            --bs-table-hover-color: var(--bs-body-color);
            --bs-table-hover-bg: rgba(0, 0, 0, 0.075);
            width: 100%;
            margin-bottom: 1rem;
            color: var(--bs-table-color);
            vertical-align: top;
            border-color: var(--bs-table-border-color);
          }
          .table > :not(caption) > * > * {
            padding: 0.5rem 0.5rem;
            background-color: var(--bs-table-bg);
            border-bottom-width: 1px;
            box-shadow: inset 0 0 0 9999px var(--bs-table-accent-bg);
          }
          .table > tbody {
            vertical-align: inherit;
          }
          .table > thead {
            vertical-align: bottom;
          }
          
          .table-group-divider {
            border-top: 2px solid currentcolor;
          }
          
          .caption-top {
            caption-side: top;
          }
          
          .table-sm > :not(caption) > * > * {
            padding: 0.25rem 0.25rem;
          }
          
          .table-bordered > :not(caption) > * {
            border-width: 1px 0;
          }
          .table-bordered > :not(caption) > * > * {
            border-width: 0 1px;
          }
          
          .table-borderless > :not(caption) > * > * {
            border-bottom-width: 0;
          }
          .table-borderless > :not(:first-child) {
            border-top-width: 0;
          }
          
          .table-striped > tbody > tr:nth-of-type(odd) > * {
            --bs-table-accent-bg: var(--bs-table-striped-bg);
            color: var(--bs-table-striped-color);
          }
          
          .table-striped-columns > :not(caption) > tr > :nth-child(even) {
            --bs-table-accent-bg: var(--bs-table-striped-bg);
            color: var(--bs-table-striped-color);
          }
          
          .table-active {
            --bs-table-accent-bg: var(--bs-table-active-bg);
            color: var(--bs-table-active-color);
          }
          
          .table-hover > tbody > tr:hover > * {
            --bs-table-accent-bg: var(--bs-table-hover-bg);
            color: var(--bs-table-hover-color);
          }
          
          .table-primary {
            --bs-table-color: #000;
            --bs-table-bg: #cfe2ff;
            --bs-table-border-color: #bacbe6;
            --bs-table-striped-bg: #c5d7f2;
            --bs-table-striped-color: #000;
            --bs-table-active-bg: #bacbe6;
            --bs-table-active-color: #000;
            --bs-table-hover-bg: #bfd1ec;
            --bs-table-hover-color: #000;
            color: var(--bs-table-color);
            border-color: var(--bs-table-border-color);
          }
          
          .table-secondary {
            --bs-table-color: #000;
            --bs-table-bg: #e2e3e5;
            --bs-table-border-color: #cbccce;
            --bs-table-striped-bg: #d7d8da;
            --bs-table-striped-color: #000;
            --bs-table-active-bg: #cbccce;
            --bs-table-active-color: #000;
            --bs-table-hover-bg: #d1d2d4;
            --bs-table-hover-color: #000;
            color: var(--bs-table-color);
            border-color: var(--bs-table-border-color);
          }
          
          .table-success {
            --bs-table-color: #000;
            --bs-table-bg: #d1e7dd;
            --bs-table-border-color: #bcd0c7;
            --bs-table-striped-bg: #c7dbd2;
            --bs-table-striped-color: #000;
            --bs-table-active-bg: #bcd0c7;
            --bs-table-active-color: #000;
            --bs-table-hover-bg: #c1d6cc;
            --bs-table-hover-color: #000;
            color: var(--bs-table-color);
            border-color: var(--bs-table-border-color);
          }
          
          .table-info {
            --bs-table-color: #000;
            --bs-table-bg: #cff4fc;
            --bs-table-border-color: #badce3;
            --bs-table-striped-bg: #c5e8ef;
            --bs-table-striped-color: #000;
            --bs-table-active-bg: #badce3;
            --bs-table-active-color: #000;
            --bs-table-hover-bg: #bfe2e9;
            --bs-table-hover-color: #000;
            color: var(--bs-table-color);
            border-color: var(--bs-table-border-color);
          }
          
          .table-warning {
            --bs-table-color: #000;
            --bs-table-bg: #fff3cd;
            --bs-table-border-color: #e6dbb9;
            --bs-table-striped-bg: #f2e7c3;
            --bs-table-striped-color: #000;
            --bs-table-active-bg: #e6dbb9;
            --bs-table-active-color: #000;
            --bs-table-hover-bg: #ece1be;
            --bs-table-hover-color: #000;
            color: var(--bs-table-color);
            border-color: var(--bs-table-border-color);
          }
          
          .table-danger {
            --bs-table-color: #000;
            --bs-table-bg: #f8d7da;
            --bs-table-border-color: #dfc2c4;
            --bs-table-striped-bg: #eccccf;
            --bs-table-striped-color: #000;
            --bs-table-active-bg: #dfc2c4;
            --bs-table-active-color: #000;
            --bs-table-hover-bg: #e5c7ca;
            --bs-table-hover-color: #000;
            color: var(--bs-table-color);
            border-color: var(--bs-table-border-color);
          }
          
          .table-light {
            --bs-table-color: #000;
            --bs-table-bg: #f8f9fa;
            --bs-table-border-color: #dfe0e1;
            --bs-table-striped-bg: #ecedee;
            --bs-table-striped-color: #000;
            --bs-table-active-bg: #dfe0e1;
            --bs-table-active-color: #000;
            --bs-table-hover-bg: #e5e6e7;
            --bs-table-hover-color: #000;
            color: var(--bs-table-color);
            border-color: var(--bs-table-border-color);
          }
          
          .table-dark {
            --bs-table-color: #fff;
            --bs-table-bg: #212529;
            --bs-table-border-color: #373b3e;
            --bs-table-striped-bg: #2c3034;
            --bs-table-striped-color: #fff;
            --bs-table-active-bg: #373b3e;
            --bs-table-active-color: #fff;
            --bs-table-hover-bg: #323539;
            --bs-table-hover-color: #fff;
            color: var(--bs-table-color);
            border-color: var(--bs-table-border-color);
          }
          
          .table-responsive {
            overflow-x: auto;
            -webkit-overflow-scrolling: touch;
          }
          
          @media (max-width: 575.98px) {
            .table-responsive-sm {
              overflow-x: auto;
              -webkit-overflow-scrolling: touch;
            }
          }
          @media (max-width: 767.98px) {
            .table-responsive-md {
              overflow-x: auto;
              -webkit-overflow-scrolling: touch;
            }
          }
          @media (max-width: 991.98px) {
            .table-responsive-lg {
              overflow-x: auto;
              -webkit-overflow-scrolling: touch;
            }
          }
          @media (max-width: 1199.98px) {
            .table-responsive-xl {
              overflow-x: auto;
              -webkit-overflow-scrolling: touch;
            }
          }
          @media (max-width: 1399.98px) {
            .table-responsive-xxl {
              overflow-x: auto;
              -webkit-overflow-scrolling: touch;
            }
          }
    </style>
  </head>
  <body>
    <h1>Some heading</h1>
    <table class="table table-striped">
        <thead>
          <tr>
            <th>Sarake 1</th>
            <th>Sarake 2</th>
            <th>Sarake 3</th>
            <th>Sarake 4</th>
            <th>Sarake 5</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
        </tbody>
    </table>
    <h1>Some heading</h1>
    <table class="table table-striped">
        <thead>
          <tr>
            <th>Sarake 1</th>
            <th>Sarake 2</th>
            <th>Sarake 3</th>
            <th>Sarake 4</th>
            <th>Sarake 5</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
          <tr>
            <td>Something</td>
            <td>Something</td>
            <td>Something Something Some thing Something Something thing Something Somethingthing Something Something</td>
            <td>Something</td>
            <td>Something</td>
          </tr>
        </tbody>
    </table>
  </body>
</html>
    `;

  await generatePdf(
    await inlineCss(html, { url: "./" }),
    headerFn,
    footerFn,
    "hn.pdf"
  );
})();
